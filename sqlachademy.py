from datetime import datetime

from sqlalchemy import create_engine

from sqlalchemy.ext.declarative import declarative_base

from sqlalchemy.orm import sessionmaker
from sqlalchemy import Column, Integer, String, DateTime


engine = create_engine("mariadb+pymysql://root:password@127.0.0.1:3306/words")
Base = declarative_base()

Session = sessionmaker(engine)
session = Session()

class Words(Base):
    __tablename__ = 'words'

    id = Column(Integer(), primary_key=True)
    palabra = Column(String(50))
    significado = Column(String(50))
    created_at = Column(DateTime(), default=datetime.now())

    def __str__(self):
        return self.palabra

Base.metadata.create_all(engine)


def main():


    while True:
        action = input("\n****MENU PRINCIPAL****\na) Agregar nueva palabra. \nb) Editar palabra existente. \nc) Eliminar palabra existente. \nd) Ver listado de palabras. \ne) Buscar significado de palabra. \nf) Salir.\nEscriba la letra de la accion que desea realizar:")
        if action == "a":
            while True:
                print("\n----Agregar nueva palabra----") 
                actionAdd = input("\na) Agregar palabra. \nb) Atras.\nEscriba la letra de la accion que desea realizar:")

                if actionAdd == "a":
                    while True:
                        b = False
                        newWord = input("Escriba la palabra que desea agregar o 's' para salir: ")
                        if newWord == "s":
                            break
                        else:
                            search = search_word(newWord)
                            if search != None:
                                b = True
                                print("¡La palabra ya existe en el sistema!")
                                
                        while True and not b:
                            acept = input("La nueva palabra sera '" + str(newWord)+"', ¿desea continuar? ('s' o 'n'):")
                            if acept == "s":
                                description = input("Ingrese el significado de '"+newWord+"':")
                                add_word(newWord,description)
                                break
                            elif acept == "n":
                                break
                            else:
                                print("Error: seleccione una de las opciones\n")
                        
                        option = input("¿Desea seguir agregando palabras? ('s' o 'n'):")  

                        if option == "n":
                            break
                        
                elif actionAdd == "b":
                    break
                else:
                    print("Por favor seleccione una opcion valida")

        elif action == "b":
            while True:
                print("\n----Editar palabra existente----")
                word = input("Escriba la palabra que desee editar:")
                exist = True
                search = search_word(word)
                if search == None:
                    exist = False
                if exist:
                    while True:
                        option = input("\n¿Que desea actualizar?\na) Palabra.\nb) Significado.\nSeleccione una opcion:")
                        if option == "a":
                            newWord = input("Ingrese la nueva palabra:")
                            update_word(word,newWord)
                            break
                        elif option == "b":
                            newDescription = input("Ingrese el nuevo significado:")
                            update_description(word,newDescription)
                            break
                        else:
                            print("Ingrese una opcion valida")
                    

                else: 
                    print("La palabra que desea actualizar no existe en el sistema")

                option = input("¿Desea seguir editando? ('s' o 'n'):")
                if option == "n":
                    break
                
        elif action == "c":
            while True:
                print("\n----Eliminar palabra existente----")
                word = input("Escriba la palabra que desee eliminar:")
                delete_word(word)

                option = input("¿Desea seguir eliminando? ('s' o 'n'):")
                if option == "n":
                    break

        elif action == "d":
            while True:
                print("\n----Ver listado de palabras----\nEstas son todas las palaabras agregadas actualmente:")
                words = all_words()
                for word in words:
                    print(word)
                option = input("\nr) Recargar datos.\ns) Atras.")
                
                if option == "s":
                    break
                elif option != "r":
                    print("\nSeleccione una opcion valida")
        elif action == "e":
            while True:
                print("\n----Buscar significado de palabra----")
                word = input("Escriba la palabra que desee buscar o 's' para salir:")
                if word == "s":
                    break
                
                description = search_word(word)
                if description != "" and description != None:
                    print("'"+str(word)+"': "+str(description[1]))
                    option = input("¿Desea seguir buscando? ('s' o 'n')")
                    if option == "n":
                        break
                else:
                    print("\nLa plabra '"+str(word)+"' no fue encontrada, pruebe con otra")

        elif action == "f":
            print("Saliendo del sistema...\nPrograma terminado.")
            break
        else:
            print("Favor de ingresar una opcion valida\n")
  

def add_word(newWord, description):
    
    session.add(Words(significado=str(description),palabra=str(newWord)))
    session.commit()
    session.close()
    
    print("'"+str(newWord) + "' agregada correctamente")
    
def search_word(word):

    try:
         data =  session.query(Words).filter_by(palabra=str(word)).first()
        
    except ValueError:
        data = ""
    
    return data

def all_words():

    try:
        data = session.query(Words).all()
        
    
    except ValueError:
        data = ""
    return data

def delete_word(word):
    exist = True
    search = search_word(word)
    if search == None:
        exist = False
    if exist:
        try:
            data =  session.query(Words).filter_by(palabra=str(word)).first()
            session.delete(data)
            session.commit()
            session.close()
            
            print("La palabra '"+str(word)+"´ a sido eliminada exitosamente")
        except ValueError:
            exist = True
    else:
        print("La palabra que desea eliminar no existe en el sistema")
        
def update_description(word,newDescription):

    try:
        data =  session.query(Words).filter_by(palabra=str(word)).first()
        data.significado = newDescription
        session.add(data)
        session.commit()
        session.close()

        
        print("La palabra '"+str(word)+"´ a sido actualizada exitosamente")
    except ValueError:
        print("No se pudo actualizar, intente de nuevo")



def update_word(word,newWord):

    try:
        data =  session.query(Words).filter_by(palabra=str(word)).first()
        data.palabra = newWord
        session.add(data)
        session.commit()
        session.close()

        
        print("La palabra '"+str(word)+"´ a sido actualizada exitosamente")
        
    except ValueError:
        print("No se pudo actualizar, intente de nuevo")
        
        
main() #se ejecuta el hilo principal del programa



